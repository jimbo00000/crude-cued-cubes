Crude, Cued Cubes

code, graphics: jimbo00000
music: CapsAdmin



Thanks to:

CapsAdmin - I can't get enough of this music! - https://soundcloud.com/capsadmin
Roberto Ierusalimschy for Lua - http://www.lua.org/
Mike Pall for Luajit - http://luajit.org/
kusma, emoon for Rocket - https://github.com/emoon/rocket
daurnimator for luajit help
lpghatguy for luajit help and Coeus - https://github.com/titan-studio/coeus
malkia for ufo - https://github.com/malkia/ufo
Khronos for OpenGL - https://www.opengl.org/
Dr. Claw for @party - http://atparty-demoscene.net/
iq - http://www.iquilezles.org/
BeautyPi - http://www.beautypi.com/
all @party attendees
