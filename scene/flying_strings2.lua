-- flying_strings.lua

require("util.glfont")
local mm = require("util.matrixmath")

flying_strings = {}
flying_strings.__index = flying_strings

local SquidLib = require("scene.tree4")

function flying_strings.new(...)
    local self = setmetatable({}, flying_strings)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function flying_strings:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.dataDir = nil
    self.glfont = nil
    self.towardsCenter = 0
    self.textFlatten = 1
    self.textscroll = 0
    self.strings = {}

    self.Squid = SquidLib.new()
    self.phase = 0 -- To fit with contained scene's keyframe name
    self.npts = 1000 -- To fit with contained scene's keyframe name
    self.Squid.fogDensity = .25
    self.colorSaturation = 0
    self.strings = {
        "Greetings to @party attendees",
        "Thanks Dr. Claw and @party team!",
    }

    self.strings_in_space = {
        {"Thank you @party organizers!", {-.9,.3,0}, 0},
        {"Greetings @party attendees!", {-15.9,.3,0}, 0},
        --{"Buy Dr.Claw a beer", {-.9,.7,0}, 0},
    }
    self.flashing_strings_in_space = {
        {"Buy", {-1.2,.7,0}, 0},
        {"Dr.", {-.5,.7,0}, 0},
        {"Claw", {.1,.7,0}, 0},
        {"a", {0.9,.7,0}, 0},
        {"beer", {1.3,.7,0}, 0},
    }
end

function flying_strings:setDataDirectory(dir)
    self.dataDir = dir
end

function flying_strings:initGL()
    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new('amiga_forever.fnt', 'amiga_forever_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
    self.Squid:initGL()
end

function flying_strings:exitGL()
    self.glfont:exitGL()
    self.Squid:exitGL()
end

function flying_strings:renderEye(model, view, proj)
    local col = {0.5,0.5,0.5}

    self.Squid.phase = self.phase
    self.Squid.npts = self.npts
    self.Squid.colorSaturation = self.colorSaturation

    local mbird = {}
    for i=1,16 do mbird[i] = model[i] end
    mm.glh_translate(mbird, 0, -.2,0)
    mm.glh_rotate(mbird, 80, 1,0,0)
    mm.glh_rotate(mbird, 30, 0,0,1)
    self.Squid:renderEye(mbird, view, proj)
    gl.glClear(GL.GL_DEPTH_BUFFER_BIT)

    for _,v in pairs(self.strings_in_space) do
        local m = {}
        for i=1,16 do m[i] = model[i] end
        local s = .004
        local tx = {}
        for i=1,3 do tx[i] = v[2][i] end
        local a = self.towardsCenter + v[3]
        a = a - math.floor(a)
        local center = {-2,-2,-10}
        for i=1,3 do
            tx[i] = a*center[i] + (1-a)*tx[i]
        end
        mm.glh_translate(m, tx[1]+self.textscroll, tx[2], tx[3])
        mm.glh_scale(m, s, -s, s)
        mm.pre_multiply(m, view)
        self.glfont:render_string(m, proj, col, v[1])
    end

    -- Hijacking this variable for 'which string to display'
    if self.textFlatten > 0 then
        local sis = self.flashing_strings_in_space[math.floor(self.textFlatten)]
        if sis then
            local v = sis
            local m = {}
            for i=1,16 do m[i] = model[i] end
            local s = .004
            local tx = {}
            for i=1,3 do tx[i] = v[2][i] end
            local a = self.towardsCenter + v[3]
            a = a - math.floor(a)
            local center = {-2,-2,-10}
            for i=1,3 do
                tx[i] = a*center[i] + (1-a)*tx[i]
            end
            mm.glh_translate(m, tx[1], tx[2], tx[3])
            mm.glh_scale(m, s, -s, s)
            mm.pre_multiply(m, view)
            self.glfont:render_string(m, proj, col, v[1])
        end
    end

end

function flying_strings:render_for_one_eye(view, proj)
end

function flying_strings:timestep(absTime, dt)
    local t = .4 * absTime
    --self.towardsCenter = t - math.floor(t)
    self.Squid:timestep(absTime, dt)
end

return flying_strings
