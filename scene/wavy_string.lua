-- wavy_string.lua

require("util.glfont")
local mm = require("util.matrixmath")

wavy_string = {}
wavy_string.__index = wavy_string

function wavy_string.new(...)
    local self = setmetatable({}, wavy_string)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function wavy_string:init()
    self.vbos = {}
    self.vao = 0
    self.prog = 0
    self.dataDir = nil
    self.glfont = nil
    self.strings = {
        "Greetings to @party attendees",
        "Thanks Dr. Claw and @party team!",
    }
    self.textscroll = 0
    self.phase = 0
end

function wavy_string:setDataDirectory(dir)
    self.dataDir = dir
end

function wavy_string:initGL()
    dir = "fonts"
    if self.dataDir then dir = self.dataDir .. "/" .. dir end
    self.glfont = GLFont.new('amiga_forever.fnt', 'amiga_forever_0.raw')
    self.glfont:setDataDirectory(dir)
    self.glfont:initGL()
end

function wavy_string:exitGL()
    self.glfont:exitGL()
end

function wavy_string:renderEye(model, view, proj)
    local col = {1, 1, 1}

    local str = self.strings[1]
    local scr = self.textscroll
    if scr > 90 then -- Hacky parameter overload - switch string with +100
        str = self.strings[2]
        scr = scr - 100
    end
    for i=1,string.len(str) do
        local ch = string.sub(str,i,i)

        local m = {}
        for i=1,16 do m[i] = model[i] end
        local s = .005

        local tx = {i/3-scr,0,0}

        local ampl = 0.13
        local freq = 2.3
        tx[1] = tx[1] + ampl*math.sin(freq*(scr+5*i))
        tx[2] = tx[2] + ampl*math.cos(freq*(scr+5*i))

        mm.glh_translate(m, tx[1], tx[2], tx[3])
        mm.glh_scale(m, s, -s, s)
        mm.pre_multiply(m, view)
        self.glfont:render_string(m, proj, col, ch)
    end
end

function wavy_string:timestep(absTime, dt)
    local t = .4 * absTime
    --self.textscroll = self.textscroll + dt
end

return wavy_string
