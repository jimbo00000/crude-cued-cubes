--[[ obj.lua

    Utility class for loading Wavefront obj files.
    Ignores face normal and tex coord indices.
]]
objfile = {}
objfile.__index = objfile

-- and its new function
function objfile.new(...)
    local self = setmetatable({}, objfile)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function objfile:init(strings)
    self.olist = {}
    self.vertlist = {}
    self.normlist = {}
    self.texlist = {}
    self.idxlist = {}
end

-- http://wiki.interfaceware.com/534.html
function string_split(s, d)
    local t = {}
    local i = 0
    local f
    local match = '(.-)' .. d .. '()'
    
    if string.find(s, d) == nil then
        return {s}
    end
    
    for sub, j in string.gmatch(s, match) do
        i = i + 1
        t[i] = sub
        f = j
    end
    
    if i ~= 0 then
        t[i+1] = string.sub(s, f)
    end
    
    return t
end

function objfile:loadmodel(filename, calcnorms)
    local basevert = 1
    local numverts = 0

    local function addobject()
        numverts = #self.idxlist - basevert - 1
        if #self.vertlist > 0 then
            print("Object", numverts, basevert)
            table.insert(self.olist, {numverts, basevert})
        end
        basevert = #self.idxlist
    end

    local inp = io.open(filename, "r")
    if inp then
        for line in inp:lines() do
            local toks = {}
            for w in string.gmatch(line, "%g+") do
                table.insert(toks, w)
            end

            local t = table.remove(toks, 1)
            local func_table = {
                ['o'] = addobject,
                ['v'] = function (x)
                    table.insert(self.vertlist, tonumber(toks[1]))
                    table.insert(self.vertlist, tonumber(toks[2]))
                    table.insert(self.vertlist, tonumber(toks[3]))
                end,
                ['vn'] = function (x)
                    table.insert(self.normlist, tonumber(toks[1]))
                    table.insert(self.normlist, tonumber(toks[2]))
                    table.insert(self.normlist, tonumber(toks[3]))
                end,
                ['vt'] = function (x)
                    table.insert(self.texlist, tonumber(toks[1]))
                    table.insert(self.texlist, tonumber(toks[2]))
                    table.insert(self.texlist, tonumber(toks[3]))
                end,
                ['l'] = function (x)
                    for i in pairs(toks) do
                        table.insert(self.idxlist, i)
                    end
                end,
                ['f'] = function (x)
                    --print(toks)
                    for i,v in pairs(toks) do
                        --print(i,v)
                        local split = string_split(v, "/")
                        for x,y in pairs(split) do
                            --print("  "..y)
                        end
                        --print(split[1])
                        table.insert(self.idxlist, tonumber(split[1])-1)
                    end
                end,
            }
            local f = func_table[t]
            if f then f() end
        end
        assert(inp:close())
        addobject()

        --if calcnorms then self:calcNormals() end
    end
end

-- Split up tris
function objfile:calcNormals()
    print("Calculating normals...")

    local verts = {}
    local norms = {}
    for i=1,#self.idxlist/3-3 do
        local a,b,c = self.idxlist[3*i-2], self.idxlist[3*i], self.idxlist[3*i-2]
        a = a+1
        b = b+1
        c = c+1
        print(i,':',a,b,c)


    end

    self.vertlist = verts
    self.normlist = norms
end
